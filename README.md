# goInstantBBB-WebUI

[![status-badge](https://ci.codeberg.org/api/badges/JohnWalkerx/goInstantBBB-WebUI/status.svg)](https://ci.codeberg.org/JohnWalkerx/goInstantBBB-WebUI)

This is the WebUI for the [goInstantBBB backend](https://codeberg.org/JohnWalkerx/goInstantBBB) build with [Svelte](https://svelte.dev).

It is a complete client-side application which runs in the browser and send requests to the goInstantBBB backend API endpoint.

Normally this WebUI gets packaged into the goInstantBBB docker image. So you don't have to install it separately.

## Get started for development

Install the dependencies...

```bash
cd goInstantBBB-WebUI
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:8080](http://localhost:8080). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

**Note:** Because this app is normally served by the backend, you need to specify the base URL of the backend.
At the moment you need to set the `baseUrl` property in the [main.js](src/main.js) to the backend.
If you run the backend locally you need to set it like this: `http://localhost:3000/`

If you're using [Visual Studio Code](https://code.visualstudio.com/) we recommend installing the official extension [Svelte for VS Code](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode). If you are using other editors you may need to install a plugin in order to get syntax highlighting and intellisense.

## Building for production mode

To create an optimised version of the app:

```bash
npm run build
```

Then take the `public` directory and serve it by a webserver.
