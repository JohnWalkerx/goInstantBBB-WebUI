import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		// Set specific baseUrl e.g. http://localhost:3000/ (Don't forget the trailing slash)
		baseUrl: ''
	}
});

export default app;