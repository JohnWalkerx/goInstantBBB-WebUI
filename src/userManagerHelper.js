export async function getAllUsers(baseUrl, jwt) {
    const res = await fetch(baseUrl + "api/v1/auth/users", {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + jwt,
        },
    });

    const response = await res.json();

    if (res.ok) {
        if (!response.hasOwnProperty("Users"))
        {
            console.error("Didn't get users");
            return undefined;
        }

        return response.Users;
    }

    return undefined;
}

export async function deleteUserRequest(baseUrl, jwt, username) {
    const requestObject = {
        Username: username
    }

    const res = await fetch(baseUrl + "api/v1/auth/user/delete", {
        method: "DELETE",
        body: JSON.stringify(requestObject),
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + jwt,
        },
    });

    const response = await res.json();

    if (res.ok) {
        if (!response.hasOwnProperty("Username"))
        {
            console.error("User '", username, "' wasn't delete properly");
            return undefined;
        }

        return response.Username;
    }

    return undefined;
}

export async function createUserRequest(baseUrl, jwt, username, password) {
    const requestObject = {
        Username: username,
        Password: password
    }

    const res = await fetch(baseUrl + "api/v1/auth/user/create", {
        method: "POST",
        body: JSON.stringify(requestObject),
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + jwt,
        },
    });

    const response = await res.json();

    if (res.ok) {
        if (!response.hasOwnProperty("Username"))
        {
            console.error("User '", username, "' wasn't created properly");
            return undefined;
        }

        return response.Username;
    }

    return undefined;
}