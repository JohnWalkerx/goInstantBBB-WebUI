import { writable } from 'svelte/store';

export const userIsLoggedIn = writable(false);
export const loggedInUsername = writable("");
export const loggedInJwt = writable("");