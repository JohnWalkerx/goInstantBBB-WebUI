export function parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};

export async function getBbbServerState(baseUrl, jwt) {
    const res = await fetch(baseUrl + "api/v1/bbbserver", {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + jwt,
        },
    });

    const response = await res.json();

    if (res.ok) {
        return response;
    }

    return undefined;
}

export function convertBbbServerStateToString(serverState) {
    switch (serverState) {
        case 0:
            return "Unknown";
        case 1:
            return "Not Running";
        case 2:
            return "Starting Up";
        case 3:
            return "Running";
        case 4:
            return "Shutting Down";
        default:
            return "Unknown server state: " + serverState;
    }
}

export async function startServer(baseUrl, jwt) {
    const res = await fetch(baseUrl + "api/v1/bbbserver", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + jwt,
        },
    });

    const response = await res.json();

    if (response !== undefined) {
        return response;
    }

    return undefined;
}

export async function stopServer(baseUrl, jwt) {
    const res = await fetch(baseUrl + "api/v1/bbbserver", {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + jwt,
        },
    });

    const response = await res.json();

    if (response !== undefined) {
        return response;
    }

    return undefined;
}